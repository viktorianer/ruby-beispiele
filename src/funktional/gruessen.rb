# frozen_string_literal: true

sprache = 'japanisch'
gruss = case sprache
        when 'deutsch'   then 'Hallo Welt'
        when 'englisch'  then 'Hello, world'
        when 'japanisch' then 'konnichiwa sekai'
        else raise 'undefinierte Sprache'
        end

puts gruss

print_function = ->(item) { puts item + 1 }
[1, 2, 3].each(&print_function)
