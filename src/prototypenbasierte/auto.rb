# frozen_string_literal: true

auto1 = Object.new
def auto1.beschleunigen
  puts 'brumm brumm brumm'
end

auto1.beschleunigen

auto2 = auto1.clone
def auto2.bremsen
  puts 'quietsch'
end

auto2.beschleunigen
auto2.bremsen
