# frozen_string_literal: true

[1, 1, 3, 5] & [3, 2, 1]
# => [1, 3]

%w[a b b z] & %w[a b c]
# => ["a", "b"]

# Can now be written as:
[1, 1, 3, 5].intersection([3, 2, 1])
# => [1, 3]

%w[a b b z].intersection(%w[a b c])
# => ["a", "b"]
