# frozen_string_literal: true

class Ente
  def beschreibung
    'Eine graue Ente'
  end

  def sprechen
    'Quak!'
  end
end

class Kuh
  def beschreibung
    'Eine dicke Kuh'
  end

  def sprechen
    'Muuuh!'
  end
end

def lass_sprechen(tier)
  puts "#{tier.beschreibung} macht: #{tier.sprechen}"
end

lass_sprechen Ente.new
lass_sprechen Kuh.new

array = [Ente.new, Kuh.new]

array.each do |tier|
  lass_sprechen tier
end
