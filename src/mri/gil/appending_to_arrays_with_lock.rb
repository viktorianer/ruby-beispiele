# frozen_string_literal: true

array = []
mutex = Mutex.new

5.times.map do
  Thread.new do
    # make key operations atomic
    mutex.synchronize do
      1000.times do
        array << nil
      end
    end
  end
end.each(&:join)

puts array.size

# rbenv local 2.4.1
# ruby src/mri/gil/appending_to_arrays_with_lock.rb
#
# 5000

# rbenv local jruby-9.2.5.0
# jruby src/mri/gil/appending_to_arrays_with_lock.rb
#
# 4292
