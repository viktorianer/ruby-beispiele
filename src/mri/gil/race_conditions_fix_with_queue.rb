# frozen_string_literal: true

class Sheep
  def initialize
    @shorn = false
  end

  def shorn?
    @shorn
  end

  def shear!
    puts 'shearing...'
    @shorn = true
  end
end

sheep = Sheep.new
sheep_queue = Queue.new
sheep_queue << sheep

# eliminated the race condition in this code
5.times.map do
  Thread.new do
    sheep = sheep_queue.pop(true)
    sheep.shear!
  rescue ThreadError
    # raised by Queue#pop in the threads
    # that don't pop the sheep
  end
end.each(&:join)

# rbenv local 2.4.1
# ruby src/gil/race_conditions_fix_with_queue.rb

# rbenv local jruby-9.2.5.0
# jruby src/gil/race_conditions_fix_with_queue.rb
#
# shearing...
