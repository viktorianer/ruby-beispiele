# frozen_string_literal: true

class Sheep
  def initialize
    @shorn = false
  end

  def shorn?
    @shorn
  end

  def shear!
    puts 'shearing...'
    @shorn = true
  end
end

sheep = Sheep.new

5.times.map do
  Thread.new do
    # a check-then-set race condition
    sheep.shear! unless sheep.shorn?
  end
end.each(&:join)

# ruby src/mri/gil/race_conditions.rb
#
# shearing...
# shearing...
# shearing...
# shearing...
# shearing...
