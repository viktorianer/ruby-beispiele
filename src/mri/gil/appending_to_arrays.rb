# frozen_string_literal: true

array = []

5.times.map do
  Thread.new do
    1000.times do
      array << nil
    end
  end
end.each(&:join)

puts array.size

# rbenv local 2.4.1
# ruby src/mri/gil/appending_to_arrays.rb
#
# 5000

# rbenv local jruby-9.2.5.0
# jruby src/mri/gil/appending_to_arrays.rb
#
# 4292
