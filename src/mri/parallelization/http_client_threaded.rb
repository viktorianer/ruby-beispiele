# frozen_string_literal: true

require 'net/http'

uri = URI('http://localhost:8000')

4.times.map do
  Thread.new do
    res = Net::HTTP.get_response uri
    puts res.body
  end
end.each(&:join)

# time ruby src/mri/parallelization/http_client_threaded.rb
#
# Hello, world!
# Hello, world!
# Hello, world!
# Hello, world!
# ruby src/mri/parallelization/http_client_threaded.rb  0.09s user 0.04s system 20% cpu 0.648 total
#
# IO#read
# Release GVL
# Wait until data is ready (select())
# Acquire GVL
