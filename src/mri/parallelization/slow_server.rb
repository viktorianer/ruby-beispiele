# frozen_string_literal: true

require 'webrick'

root = File.expand_path '~/public_html'
server = WEBrick::HTTPServer.new Port: 8000, DocumentRoot: root

trap('INT') { server.shutdown }

server.mount_proc '/' do |_req, res|
  sleep 0.5
  res.body = 'Hello, world!'
end

begin
  server.start
ensure
  server.shutdown
end

# ruby src/mri/parallelization/slow_server.rb
