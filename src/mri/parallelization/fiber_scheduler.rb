# frozen_string_literal: true

# https://www.youtube.com/watch?v=Y29SSOS4UOc

require 'async'
require 'net/http'
require 'uri'

Async do
  %w[ruby rails async].each do |topic|
    Async do
      Net::HTTP.get(URI("https://www.google.com/search?q=#{topic}"))
    end
  end
end
