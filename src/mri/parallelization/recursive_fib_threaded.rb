# frozen_string_literal: true

def fib(num)
  if num < 3
    1
  else
    fib(num - 1) + fib(num - 2)
  end
end

4.times.map do
  Thread.new { fib 34 }
end.each(&:join)

# gil replaced by gvl
# time ruby src/mri/parallelization/recursive_fib_threaded.rb
#
# ruby src/mri/parallelization/recursive_fib_threaded.rb  1.31s user 0.04s system 99% cpu 1.361 total
