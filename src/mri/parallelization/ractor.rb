# frozen_string_literal: true

def tarai(var_x, var_y, var_z)
  if var_x <= var_y
    var_y
  else
    tarai(tarai(var_x - 1, var_y, var_z),
          tarai(var_y - 1, var_z, var_x),
          tarai(var_z - 1, var_x, var_y))
  end
end

require 'benchmark'
Benchmark.bm do |x|
  # sequential version
  x.report('seq') { 4.times { tarai(14, 7, 0) } }

  # parallel version
  x.report('par') do
    4.times.map do
      Ractor.new { tarai(14, 7, 0) }
    end.each(&:take)
  end
end

# user     system      total        real
# seq 67.797254   0.334200  68.131454 ( 68.509896)
# par 80.238165   0.284107  80.522272 ( 20.362288)
# <internal:ractor>:267: warning: Ractor is experimental, and
# the behavior may change in future versions of Ruby!
# Also there are many implementation issues.
