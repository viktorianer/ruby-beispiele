# frozen_string_literal: true

require 'net/http'

uri = URI('http://localhost:8000')

4.times do
  res = Net::HTTP.get_response uri
  puts res.body
end

# time ruby src/mri/parallelization/http_client.rb
#
# Hello, world!
# Hello, world!
# Hello, world!
# Hello, world!
# ruby src/mri/parallelization/http_client.rb  0.08s user 0.04s system 5% cpu 2.148 total
