# frozen_string_literal: true

def fib(num)
  if num < 3
    1
  else
    fib(num - 1) + fib(num - 2)
  end
end

4.times { fib 34 }

# gil replaced by gvl
# time ruby src/mri/parallelization/recursive_fib.rb
#
# ruby src/mri/parallelization/recursive_fib.rb  1.29s user 0.04s system 99% cpu 1.341 total
