# frozen_string_literal: true

# test.rb
def foo(num)
  num.to_s if num > 10
end

foo(42)

# $ typeprof src/type_prof/test.rb
# # Classes
# class Object
#   def foo : (Integer) -> String?
# end
