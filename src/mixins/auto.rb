# frozen_string_literal: true

module Gaspedal
  def beschleunigen
    puts 'brumm brumm brumm'
  end
end

module Bremse
  def bremsen
    puts 'quietsch'
  end
end

# Ein Auto zusammenbauen
auto = Object.new
auto.extend Gaspedal
auto.extend Bremse

auto.beschleunigen
auto.bremsen
