# frozen_string_literal: true

# mit Klassen kombinieren
class Auto
  include Gaspedal
end

class GutesAuto < Auto
  include Bremse
end

auto1 = Auto.new
auto1.beschleunigen

auto2 = GutesAuto.new
auto2.bremsen

# mit Prototypen kombinieren
auto1 = Object.new
auto1.extend Gaspedal

auto2 = auto1.clone
auto2.extend Bremse

auto1.beschleunigen
auto2.bremsen
