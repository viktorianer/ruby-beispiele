# frozen_string_literal: true

# rubocop:disable Lint/Void, Lint/IdentityComparison, Lint/BinaryOperatorWithIdenticalOperands
# zeigen auf dasselbe Objekt im Speicher
:george.object_id == :george.object_id

# zwei verschiedene Objekte im Speicher
'george'.object_id == 'george'.object_id
# rubocop:enable Lint/Void, Lint/IdentityComparison, Lint/BinaryOperatorWithIdenticalOperands
