# frozen_string_literal: true

x = 10
y = 11
z = x < y
puts z # 'z = ' + z.to_s
