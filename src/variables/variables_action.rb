# frozen_string_literal: true

# https://stackoverflow.com/questions/15773552/ruby-class-instance-variable-vs-class-variable
require_relative '../variables/parent'
require_relative '../variables/child'

mama = Parent.new
papa = Parent.new
joey = Child.new
suzy = Child.new

Parent.family_things << :house
papa.family_things   << :vacuum
mama.shared_things   << :car
papa.shared_things   << :blender
papa.my_things       << :quadcopter
joey.my_things       << :bike
suzy.my_things       << :doll
joey.shared_things   << :puzzle
suzy.shared_things   << :blocks

puts '== family_things =='
puts "Parent:\t #{Parent.family_things}" #=> [:house, :vacuum]
puts "Child:\t #{Child.family_things}" #=> [:house, :vacuum]
puts "papa:\t #{papa.family_things}"   #=> [:house, :vacuum]
puts "mama:\t #{mama.family_things}"   #=> [:house, :vacuum]
puts "joey:\t #{joey.family_things}"   #=> [:house, :vacuum]
puts "suzy:\t #{suzy.family_things}"   #=> [:house, :vacuum]

puts '== shared_things =='
puts "Parent:\t #{Parent.shared_things}" #=> [:car, :blender]
puts "papa:\t #{papa.shared_things}"   #=> [:car, :blender]
puts "mama:\t #{mama.shared_things}"   #=> [:car, :blender]
puts "Child:\t #{Child.shared_things}" #=> [:puzzle, :blocks]
puts "joey:\t #{joey.shared_things}"   #=> [:puzzle, :blocks]
puts "suzy:\t #{suzy.shared_things}"   #=> [:puzzle, :blocks]

puts '== my_things =='
puts "papa:\t #{papa.my_things}"       #=> [:quadcopter]
puts "mama:\t #{mama.my_things}"       #=> []
puts "joey:\t #{joey.my_things}"       #=> [:bike]
puts "suzy:\t #{suzy.my_things}"       #=> [:doll]

# ruby src/variables/variables_action.rb
