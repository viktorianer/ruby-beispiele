# frozen_string_literal: true

def square(num) = num * num
