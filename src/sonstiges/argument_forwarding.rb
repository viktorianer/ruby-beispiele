# frozen_string_literal: true

class Greeter
  def hello(argument)
    puts "Hello #{argument}"
  end
end

def hello(...)
  Greeter.new.hello(...)
end

hello 'Ruby'

# By the way, arguments forwarding now supports leading arguments.
# rubocop:disable Style/MissingRespondToMissing
def method_missing(meth, ...)
  send(:"do_#{meth}", ...)
end
# rubocop:enable Style/MissingRespondToMissing
