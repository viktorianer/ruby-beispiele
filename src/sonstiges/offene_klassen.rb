# frozen_string_literal: true

class Integer
  def hours
    self * 3600 # Anzahl der Sekunden in einer Stunde
  end
  alias hour hours
end

# 14 Stunden nach 00:00 am 1. Januar
# (oder "als du endlich aufwachst" ;)
Time.mktime(2006, 0o1, 0o1) + 14.hours  #-> Sun Jan 01 14:00:00
