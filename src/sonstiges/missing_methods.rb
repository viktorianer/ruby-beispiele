# frozen_string_literal: true

# 'id' ist der Name der aufgerufenen Methode, ein * sammelt
# alle argumente in einem Array namens 'arguments'.
# rubocop:disable Style/MissingRespondToMissing
def method_missing(id, *arguments)
  puts "Die Methode #{id} wurde nicht gefunden."
  puts "Folgende Argumente wurden übergeben: #{arguments.join(', ')}"
end
# rubocop:enable Style/MissingRespondToMissing

__ :a, :b, 10
#-> Die Methode __ wurde nicht gefunden.
#-> Folgende Argumente wurden uebergeben: a, b, 10
