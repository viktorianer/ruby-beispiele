# frozen_string_literal: true

class Test
  # standardmaessig "public"
  def identifier
    99
  end

  # [], []=, +@, -@
  # =, .., ..., not, &&, and, ||, or, ::
  def ==(other)
    identifier == other.identifier
  end
end

t1 = Test.new  # => #<Test:0x34ab50>
t2 = Test.new  # => #<Test:0x342784>
# rubocop:disable Lint/Void
t1 == t2 # => true

# `identifier' ist jetzt "protected"; das funktioniert noch,
# weil `other' eine Instanz derselben Klasse ist

class Test
  protected :identifier
end

t1 == t2 # => true

# nun ist `identifier' "private"

class Test
  private :identifier
end

t1 == t2
# NoMethodError: private method `identifier' called for #<Test:0x342784>
# rubocop:enable Lint/Void
