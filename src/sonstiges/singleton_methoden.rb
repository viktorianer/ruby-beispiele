# frozen_string_literal: true

class Car
  def inspect
    'billig'
  end
end

porsche = Car.new
porsche.inspect  #-> billig

def porsche.inspect
  'teuer'
end

porsche.inspect  #-> teuer

# Andere Objekte werden nicht beeinflusst:
car = Car.new
car.inspect  #-> billig
