# frozen_string_literal: true

class Hello
  def self.hello_world
    puts 'Hello, World!'
  end
end

Hello.hello_world
