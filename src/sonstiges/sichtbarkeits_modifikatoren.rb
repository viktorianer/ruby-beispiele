# frozen_string_literal: true

class MyPrivateClass
  private

  def a_method
    true
  end

  def another_method
    false
  end
end

class MyPublicClass
  # Jetzt ist a_method "public".
  def a_method
    true
  end

  # Jetzt ist a_method wieder "private".
  private :a_method

  private

  # another_method ist "private".
  def another_method
    false
  end
end
