# frozen_string_literal: true

# Celsius degrees
case temperature
when ..-15
  puts 'Deep Freeze'
when -15..8
  puts 'Refrigerator'
when 8..15
  puts 'Cold'
when 15..25
  puts 'Room Temperature'
when (25..) # Kindly notice the brackets here
  puts 'Hot'
end

TEMPERATURE = {
  ..-15 => :deep_freeze,
  -15..8 => :refrigerator,
  8..15 => :cold,
  15..25 => :room_temperature,
  25.. => :hot
}.freeze

# In Rails
User.where(created_at: (..DateTime.now))
# User Load (2.2ms)  SELECT "users".* FROM "users" WHERE "users"."created_at"
# <= $1 LIMIT $2  [["created_at", "2020-08-05 15:00:19.111217"], ["LIMIT", 11]]

# In RubySpec
ruby_version(..'1.9') do
  # Tests for old Ruby
end
