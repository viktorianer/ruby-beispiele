# frozen_string_literal: true

def deliver(from: 'A', to: nil, via: 'mail')
  "Sending from #{from} to #{to} via #{via}."
end

puts deliver(to: 'B')
# => "Sending from A to B via mail."
puts deliver(via: 'Pony Express', from: 'B', to: 'A')
# => "Sending from B to A via Pony Express."
