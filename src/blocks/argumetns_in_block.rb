# frozen_string_literal: true

[1, 2, 10].map { _1.to_s(16) }
#=> ["1", "2", "a"]

[1, 2, 3].map { _1 * 3 }
# => [3, 6, 9]

# rubocop:disable Lint/UnexpectedBlockArity
[1, 2].each_with_index { p _1 }
# 1
# 2
# => [1, 2]

[1, 2].each_with_index { p _2 }
# 0
# 1
# => [1, 2]
# rubocop:enable Lint/UnexpectedBlockArity

# Shorter one-liners?
hash = {
  key1: '1',
  key2: '2'
}
hash.map { [_1.to_s, _2.to_i] }.to_h
# => { "key1" => 1, "key2" => 2 }

# Simpler simple-one-lines that can't be otherwise written?
[1, 2, 3].collect { _1**2 }

# Shorter definitions of less important variables?
h = Hash.new { _1[_2] = [] }
h[:a].push(1)

# Cleaner method chains?
require 'net/http'
require 'json'

uri = URI('http://localhost:8000')
Net::HTTP.get(uri).body.then { JSON.parse(_1, symbolize_names: true) }

# In Rails
# Weird each with objects?
%i[method1 method2 method3].each_with_object(Post.find(1)) { _2.send(_1) }
