# frozen_string_literal: true

array = [1, 2, 3]

array.each do |element|
  puts element
end

def mach_zweimal
  yield
  yield
end

mach_zweimal { puts 'Block aufgerufen!' }

5.times do |element|
  puts element
end

def block(&the_block)
  # der Block, der an die Methode uebergeben wurde
  the_block # Gib den Block zurueck.
end

adder = block { |a, b| a + b }
# adder ist jetzt ein Proc
adder.class #-> Proc

adder.block
# NoMethodError (private method `block' called for #<Proc:0x00007f8985159c88 (irb):5>)

adder.call(1, 2)

method(:puts).call 'puts ist ein Objekt!'
#-> puts ist ein Objekt!
