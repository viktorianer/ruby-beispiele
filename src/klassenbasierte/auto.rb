# frozen_string_literal: true

# automatisch von der Mutterklasse Object.
class Auto
  def beschleunigen
    puts 'brumm brumm brumm'
  end
end

auto1 = Auto.new
auto1.beschleunigen

# von einer anderen Klasse als Object
class GutesAuto < Auto
  def bremsen
    puts 'quietsch'
  end
end

auto2 = GutesAuto.new
auto2.beschleunigen
auto2.bremsen

SchnellesAuto = Class.new do
  attr_accessor :speed
end

auto3 = SchnellesAuto.new
auto3.speed = 200
puts auto3.speed
