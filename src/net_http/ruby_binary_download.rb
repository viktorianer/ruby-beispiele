#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'

class RubyBinary
  def download(ruby_version = '2.3.0', destination_dir = 'tmp')
    Dir.mkdir destination_dir unless File.directory? destination_dir
    path = "#{destination_dir}/#{ruby_version}"
    if File.file? path
      puts "Ruby #{ruby_version} is already downloaded to #{path}."
    else
      response = download_ruby(ruby_version)
      write_to_file(response, ruby_version, path)
    end
  end

  private

  def download_ruby(ruby_version)
    ruby = ruby_version.split('.')
    http = Net::HTTP.start('cache.ruby-lang.org')
    puts "Downloading ruby #{ruby_version} from
      cache.ruby-lang.org/pub/ruby/#{ruby[0]}.#{ruby[1]}/ruby-#{ruby_version}.tar.gz..."
    http.get("/pub/ruby/#{ruby[0]}.#{ruby[1]}/ruby-#{ruby_version}.tar.gz")
  end

  def write_to_file(response, ruby_version, path)
    if response.is_a?(Net::HTTPSuccess)
      File.open(path, 'wb').write(response.read_body)
      puts "Saved ruby #{ruby_version} to #{path}."
    else
      puts "Ruby #{ruby_version} was not found. Please provide valid version."
    end
  end
end

ruby = RubyBinary.new
ruby.download('2.5.1', 'tmp')
