# frozen_string_literal: true

# rubocop:disable Lint/FlipFlop
(1..10).each { |i| puts i if (i == 5) .. (i == 8) }
# rubocop:enable Lint/FlipFlop
# 5
# 6
# 7
# 8
# => 1..10
